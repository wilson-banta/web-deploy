var ssm_command = require('../lib/run_ssm_command');
var assert = require('chai').assert;

describe('run_ssm_command', function() {

    this.timeout(100000);

    describe('echo-hello', function() {

        it('should be able to run the hello commnad', function(cb) {
            ssm_command.run({
                commandName: 'STA-Ping',
                commandComment: 'Testing the echo hello command',
                instanceStaAppsTagValues: 'Testing,Test'
            }, cb)
        });
    });

    describe('fail', function() {
        it('should run and return an error', function(cb) {
            //assert.throws(function() {
            ssm_command.run({
                    commandName: 'STA-Throw-An-Error',
                    commandComment: 'Testing the echo hello command'
                }, function(e) {
                    if (e) throw e

                    cb(e);
                })
                //}, "Error thrown")
        });
    });


});

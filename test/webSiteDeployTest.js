var assert = require('better-assert');
var sinon = require('sinon');
var loadBalancer = require('../lib/utils/loadBalancer');
var installer = require('../lib/utils/packageInstaller');

var webDeployer = require('../lib/websiteDeploy');

function createValidRequest() {
    return {
        loadBalancerExternalPort: 80,
        loadBalanceInstanceGreenPort: 8010,
        loadBalanceInstanceBluePort: 8020,
        environment: 'test',
        loadBalancerName: 'test',
        packages: [{
            test: 'x'
        }]
    };
}
describe('websiteDeploy', function() {

    var validRequest, installOptions, loadBalancerOptions, currentloadBalanceInstance;

    beforeEach(function() {
        validRequest = createValidRequest();

        sinon.stub(installer, 'install', function(options, cb) {
            installOptions = options;
            return cb();
        });

        sinon.stub(loadBalancer, 'getInstancePort', function(options, cb) {
            loadBalancerOptions = options;
            return cb(null, currentloadBalanceInstance);
        });
    });

    describe('parameter error checks', function() {

        function performRequest(errorMessage) {
            if (!doesThrow(() => webDeployer(validRequest), errorMessage))
                throw errorMessage;
        }

        it('should throw if external load balanacer port not supplied', function() {
            validRequest.loadBalancerExternalPort = null;
            performRequest('loadBalancerExternalPort must be supplied');

        });

        it('should throw if packages not supplied', function() {
            validRequest.packages = null;
            performRequest('Packages to install must be supplied');

        });

        it('should throw if blue sub environment port not supplied', function() {
            validRequest.loadBalanceInstanceBluePort = null;
            performRequest('loadBalanceInstanceBluePort must be supplied');

        });

        it('should throw if green sub environment port not supplied', function() {
            validRequest.loadBalanceInstanceGreenPort = null;
            performRequest('loadBalanceInstanceGreenPort must be supplied');
        });


        it('should throw if environment is not supplied', function() {
            validRequest.environment = null;
            performRequest('environment must be supplied');
        });

        it('should throw if loadBalancerName is not supplied', function() {
            validRequest.loadBalancerName = null;
            performRequest('loadBalancerName must be supplied');
        });
    });

    describe('sub environment switch', function() {

        it('should set next port to blue if current is green', function(cb) {

            currentloadBalanceInstance = validRequest.loadBalanceInstanceBluePort;

            webDeployer(validRequest, function() {
                assert(installOptions.port  == validRequest.loadBalanceInstanceGreenPort);
                assert('Green' == validRequest.subEnvironmentName);
                assert(loadBalancerOptions.loadBalancerName== validRequest.loadBalancerName);
                assert(loadBalancerOptions.externalLoadBalancerPort== validRequest.loadBalancerExternalPort);

                cb();
            })
        });

        it('should set next port to green if current is blue', function(cb) {

            currentloadBalanceInstance = validRequest.loadBalanceInstanceGreenPort;

            webDeployer(validRequest, function() {
                assert(installOptions.port == validRequest.loadBalanceInstanceBluePort);
                assert('Blue' == validRequest.subEnvironmentName);
                assert(loadBalancerOptions.loadBalancerName== validRequest.loadBalancerName);
                assert(loadBalancerOptions.externalLoadBalancerPort== validRequest.loadBalancerExternalPort);
                cb();
            })

        });
    });

    afterEach(function() {
        installer.install.restore();
        loadBalancer.getInstancePort.restore();
    });
});

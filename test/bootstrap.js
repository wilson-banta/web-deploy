

//run();
global.doesThrow = (operation, error) => {
    try {
        operation();
        console.log('ERROR NOT THROWN!')
        return false;
    } catch (e) {
        console.log(e);
        console.log(error);
        return e == error || e.message == error;
    }
}

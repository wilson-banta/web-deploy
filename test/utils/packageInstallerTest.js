var packageInstaller = require('../../lib/utils/packageInstaller');
var assert = require('better-assert');
var sinon = require('sinon');
var ssm_command = require('../../lib/run_ssm_command');

var validRequest;

function createValidRequest() {
    return {
        packageName: 'PublicSite',
        port: 8010,
        environment: 'develop'
    };
}


describe('packageInstaller', function() {
    this.timeout(60000);

    beforeEach(function() {
        validRequest = createValidRequest();
    });

    describe('install', () => {
        describe('input validation', () => {

            function performRequest(errorMessage) {
                if (!doesThrow(() => packageInstaller.install(validRequest), errorMessage)) throw errorMessage;
            }

            it("should require packageName", () => {
                validRequest.packageName = null;
                performRequest('packageName must be supplied');
            });

            it("should require environment", () => {
                validRequest.environment = null;
                performRequest('environment must be supplied');
            });
        });

        describe('valid request', () => {

            var ssmOptions;

            before(function() {
                sinon.stub(ssm_command, 'run', function(options, cb) {
                    ssmOptions = options;
                    return cb();
                });
            });

            it("should return the internal port", (cb) => {

                this.timeout(60000);

                packageInstaller.install(validRequest, function(err) {
                    assert(ssmOptions.commandName == 'STA-Prazzle-Deploy-Package');
                    assert(ssmOptions.commandComment == `Running: ${validRequest.packageName} - Env: ${validRequest.environment} `);
                    assert(ssmOptions.parameters.environment[0] == validRequest.environment);
                    assert(ssmOptions.parameters.packageName[0] == validRequest.packageName);

                    cb(err);
                });
            });
        });
    });
});

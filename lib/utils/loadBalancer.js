var async = require('async');
var AWS = require('aws-sdk');
var elb = new AWS.ELB();
var async = require('async');

exports.getInstancePort = function(options, cb) {

    if (!options) throw new Error('Options must be supplied');
    if (!options.loadBalancerName) throw new Error('loadBalancerName must be supplied');
    if (!options.loadBalancerExternalPort) throw new Error('loadBalancerExternalPort must be supplied');

    //  get all load balances in the aws account.
    elb.describeLoadBalancers({}, function(err, data) {
        if (err) return cb(err);

        var listener1, listener2;

        for (var i = 0; i < data.LoadBalancerDescriptions.length; i++) {
            var balancer = data.LoadBalancerDescriptions[i];

            if (balancer.LoadBalancerName != options.loadBalancerName) continue;

            for (var j = 0; j < balancer.ListenerDescriptions.length; j++) {
                var listener = balancer.ListenerDescriptions[j].Listener;

                if (listener.LoadBalancerPort == options.loadBalancerExternalPort) {
                    listener1 = listener;
                }
                if (listener.LoadBalancerPort == options.loadBalancerExternalPort2) {
                    listener2 = listener;
                }

                if (listener1 && !options.loadBalancerExternalPort2)
                    return cb(null, listener.InstancePort, listener1, listener2, balancer);

                if (listener1 && listener2)
                    return cb(null, listener.InstancePort, listener1, listener2, balancer);
            }

        }

        cb(`Failed to find listners. loadBalancerName: ${options.loadBalancerName}, External Port: ${options.loadBalancerExternalPort} `);


    });
}

exports.switchInstancePort = function(options, cb) {

    if (!options) throw new Error('Options must be supplied');
    if (!options.loadBalancerName) throw new Error('loadBalancerName must be supplied!');
    if (!options.loadBalancerExternalPort) throw new Error('loadBalancerExternalPort must be supplied');
    //if (!options.loadBalancerExternalPort2) throw new Error('loadBalancerExternalPort must be supplied');

    if (!options.loadBalanceInstanceGreenPort) throw new Error('loadBalanceInstanceGreenPort must be supplied');
    if (!options.loadBalanceInstanceBluePort) throw new Error('loadBalanceInstanceBluePort must be supplied');

    var context = {};

    async.waterfall([
        get_current_instance_port(options, context),
        set_new_instance_port(options, context),
        remove_existing_listener(options, context),
        create_new_listenr(options, context)
    ], cb);

}

function get_current_instance_port(options, context) {
    return cb => {
        exports.getInstancePort(options, (e, port, listener1, listener2, balancer) => {

            if (e) return cb(e);

            context.initialPort = port;
            context.listener1 = listener1;
            context.listener2 = listener2;
            context.balancer = balancer;
            cb();
        });
    };
}

function set_new_instance_port(options, context) {
    return cb => {
        context.newInstancePort = context.initialPort == options.loadBalanceInstanceGreenPort ?
            options.loadBalanceInstanceBluePort : options.loadBalanceInstanceGreenPort;

        cb();
    };
}

function remove_existing_listener(options, context) {
    return cb => {
        var params = {
            LoadBalancerName: context.balancer.LoadBalancerName,
            LoadBalancerPorts: options.loadBalancerExternalPort2 ? [options.loadBalancerExternalPort, options.loadBalancerExternalPort2] : [options.loadBalancerExternalPort]
        };
        elb.deleteLoadBalancerListeners(params, (err) => cb(err));
    };
}


function create_new_listenr(options, context) {
    return cb => {

        var otherListeners = [];


        for (var j = 0; j < context.balancer.ListenerDescriptions.length; j++) {
            var listener = context.balancer.ListenerDescriptions[j].Listener;
            if (listener.LoadBalancerPort == options.loadBalancerExternalPort ||
                listener.LoadBalancerPort == options.loadBalancerExternalPort2) {
                continue;
            }
            otherListeners.push(listener);
        }

        var params = {
            Listeners: otherListeners,
            LoadBalancerName: context.balancer.LoadBalancerName /* required */
        };

        params.Listeners.push({
            InstancePort: context.newInstancePort,
            /* required */
            LoadBalancerPort: options.loadBalancerExternalPort,
            /* required */
            Protocol: context.listener1.Protocol,
            /* required */
            InstanceProtocol: context.listener1.InstanceProtocol,
            SSLCertificateId: context.listener1.SSLCertificateId
        });

        if (options.loadBalancerExternalPort2) {
            params.Listeners.push({
                InstancePort: context.newInstancePort,
                /* required */
                LoadBalancerPort: options.loadBalancerExternalPort2,
                /* required */
                Protocol: context.listener2.Protocol,
                /* required */
                InstanceProtocol: context.listener2.InstanceProtocol,
                SSLCertificateId: context.listener2.SSLCertificateId
            });
        }

        elb.createLoadBalancerListeners(params, (err) => cb(err));
    };
}

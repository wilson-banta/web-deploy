cmd /c npm install

if %errorlevel% neq 0 exit /b %errorlevel%
cmd /c "buildkite-agent artifact download loadBalancerPort.txt ."

if %errorlevel% neq 0 exit /b %errorlevel%
set /p HIPTEST_PORT=<loadBalancerPort.txt

echo "-- Running hiptest on Port: %HIPTEST_PORT%"
%~dp0..\node_modules\.bin\buildkite.cmd build --create --access-token %ST_ACCESS_TOKEN% --message "%BUILDKITE_MESSAGE%" --branch master --commit HEAD --env "{'TEST_SITE':'%TEST_SITE%:%HIPTEST_PORT%'}" --pipeline hiptest --organization %BUILDKITE_ORGANIZATION_SLUG% --wait-result

console.log('Upload Static Resources To CDN, Current AWS_REGION: ' + process.env.AWS_REGION);

var prazzle = require('prazzle');
var spawn = require('child_process').spawn;

var async = require('async');



var environmentName = process.env.ENVIRONMENT_NAME;
if (!environmentName) throw 'An ENVIRONMENT_NAME environment variable needs to be set';

var bucketName = process.env.CDN_BUCKET_NAME;
if (!bucketName) throw 'An CDN_BUCKET_NAME environment variable needs to be set';


var context = {
    environment: environmentName
};

async.parallel([
        do_public_site_resources(context),
        do_organiser_site_resources(context)
    ],
    e => {
        if (e) throw e;
        console.log('Upload Of Resoures Complete')
    }
);

function do_public_site_resources(context) {
    return cb => {

        var siteContext = {
            environment: context.environment,
            name: 'PublicSite',
            path: 'PublicSite/static/',
            cdnPath: 'static'
        };

        async.waterfall([
            site_download(siteContext),
            site_upload_content(siteContext)
        ], cb);
    };
}

function do_organiser_site_resources(context) {
    return cb => {

        var siteContext = {
            name: 'Web',
            path: 'organiser/static',
            environment: context.environment,
            path: 'Web/Areas/Organiser/static/',
            cdnPath: 'organiser/areas/organiser/static'
        };

        async.waterfall([
            site_download(siteContext),
            site_upload_content(siteContext)
        ], cb);
    };
}


function site_download(context) {
    return cb => {
        console.log(`Downloading package: ${context.name} for: ${context.environment}`);

        prazzle.deployPackage.run({
            s3Bucket: "prazzle2",
            skipInstall: true,
            skipSecrets: true,
            name: context.name,
            noTimeBasedSubDirectory: 1,
            environment: context.environment,
            targetDir: "./tmp/" + context.name,
            packagesDir: "./tmp/_packages"
        }, cb);
    }
}

function site_upload_content(context) {
    return cb => {
        console.log(`Uploading content for: ${context.name}, path: ${context.path}`)
        execCommand(`aws`, `s3 cp --quiet --recursive ./tmp/${context.path} s3://${bucketName}/${context.cdnPath} --metadata-directive REPLACE --expires 2060-01-01T00:00:00Z --acl public-read --cache-control max-age=2592000,public`, cb);
    }
}



function execCommand(cmd, parameters, cb) {

    console.log('Running: ' + cmd + ' ' + parameters);

    var ps = spawn(cmd, parameters.split(' '));

    ps.stdout.on('data', (data) => {
        console.log(data.toString());
    });

    ps.stderr.on('data', (data) => {
        console.error(data.toString());
    });

    ps.on('close', (code) => {

        if (code !== 0) {
            return cb('An error occured during the web publish. Exit Code: ' + code);
        }

        return cb();
    });
}
